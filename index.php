<?php
    $DB_NAME = "buku";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
    $sql = "SELECT b.kode, b.judul, b.penerbit, b.tahun, k.kategori, b.photo
    FROM buku b, kategori k WHERE b.id_kat = k.id_kat";
    
    $result = mysqli_query($conn,$sql);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Harga Buku</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body class="bg-light">
    <div class="container">
        <div class="row justify-content-center mt-5">
            <h4>Daftar Buku</h4>
            <table class="mt-3 table table-bordered">
                <tr>
                    <th>Kode</th>
                    <th>Judul</th>
                    <th>Penerbit</th>
                    <th>Tahun Terbit</th>
                    <th>Kategori</th>
                    <th>Foto</th>
                    <th>Action</th>
                </tr>
                <?php 
                    while($sbk = mysqli_fetch_assoc($result)){
                ?>
                <tr>                 
                    <td><?php echo $sbk['kode']; ?></td>
                    <td><?php echo $sbk['judul']; ?></td>
                    <td><?php echo $sbk['penerbit']; ?></td>
                    <td><?php echo $sbk['tahun']; ?></td>
                    <td><?php echo $sbk['kategori']; ?></td>
                    <td><img src="images/<?php echo $sbk['photo']; ?>" style="width: 100px;" alt="Foto Buku"></td>
                    <td><?php echo "<button><a href='proses-hapus.php?kode=".$sbk['kode']."'>Hapus</a></button>"; ?></td>
                </tr> 
                <?php } ?>
            </table>
        </div>
    </div>
</body>
</html>